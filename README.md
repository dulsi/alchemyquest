# Alchemy Quest / Open Alchemist - 0.5.2

[[_TOC_]]

## Is this Open Alchemist or Alchemy Quest?

It is both. The original game was Open Alchemist. OpenGameArt user devurandom created NES artwork for the game and called it Alchemy Quest on the title screen. At the time you couldn't use the artwork in the game. We have updated the game to support both the original content and the Alchemy Quest mode. Alchemy Quest does have features beyond the original such as a two player mode.

## What Alchemy Quest needs ?

Alchemy Quest is based on [SDL2, SDL2_image, SDL2_mixer](https://www.libsdl.org), [boost](https://www.boost.org/), [expat](https://libexpat.github.io/), and [libzip](https://libzip.org/). If you want achievement support you need [libgamerzilla](http://identicalsoftware.com/gamerzilla/).
You must install these libraries before compiling this game.

You also need the "zip" program, in order to make skins archives.

## How to compile/install it

Compiling the program requires cmake.

```
cmake . && make && sudo make install".
```

## How to play with it

After compiling, type in the prompt: 
```
alchemyquest

or

alchemyquest --openalchemist
(A symbolic link with the name openalchemist will automatically invoke this mode.)
```

## How to contact us

The original creators are not involved with this modification. You can contact the current programmer at dulsi (at) identicalsoftware.com

You can contact the original coder at kepho.o (at) gmail.com
You can contact the original graphist at guigozz (at) gmail.com
Original website is : www.openalchemist.com

## Licensing

Code is protected by GNU GPL 2+ (see `CODE-LICENSE`)
Open Alchemist's graphics are protected by https://creativecommons.org/licenses/by-sa/2.0/fr/
Alchemy Quest's graphics are protected by https://creativecommons.org/licenses/by-sa/3.0/

## Derivated projects

If you want to make a derivated project with OpenAlchemist code or graphs, you can do it. But a message from you to show us your project will  be appreciated (we're curious :)).

## Special Thanks

Special thanks to silkut for his win32 port and MrPouit for his ubuntu packages.
Also thanks to the ClanLib team.

