// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : Statistics.cpp
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

/************************************************************************/
/* Includes                                                             */
/************************************************************************/
#include "Statistics.h"
#ifdef HAVE_GAMERZILLA
#include "misc.h"
#include <gamerzilla.h>
#endif

void Statistics::add_combo(int combo)
{
	if (combo > maxcombo)
		maxcombo = combo;
	totalcombo += combo;
}

void Statistics::unlock_piece(int piece)
{
	if (unlocked_pieces < piece)
	{
		unlocked_pieces = piece;
#ifdef HAVE_GAMERZILLA
		if (!alchemyQuest)
		{
			GamerzillaSetTrophyStat(game_id, "First New Element", piece);
			GamerzillaSetTrophyStat(game_id, "Most Elements", piece);
			GamerzillaSetTrophyStat(game_id, "All Elements", piece);
		}
#endif
	}
}
