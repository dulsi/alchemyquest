// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : CommonResources.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _STATISTICS_H_
#define _STATISTICS_H_

/**
* This class collect game statistics
*/
class Statistics
{
public:
	Statistics() : highscore(0), maxcombo(0), totalcombo(0), unlocked_pieces(3) {}

	void add_combo(int combo);
	void unlock_piece(int piece);

	unsigned int highscore;
	unsigned int maxcombo;
	unsigned int totalcombo;
	unsigned int unlocked_pieces;

	/** Previous highscore	*/
	unsigned int old_highscore;
};

#endif
