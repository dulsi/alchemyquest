// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : CommonResources.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _COMMON_RESOURCES_H_
#define _COMMON_RESOURCES_H_

/************************************************************************/
/* Includes                                                             */
/************************************************************************/
#include <SDL.h>
#include <zip.h>
#include "SDL_CL_Sprite.h"

#include "Board.h"
#include "HumanPlayer.h"
#include "DemoPlayer.h"
#include "FrontLayer.h"
#include "Keys.h"
#include "Statistics.h"

/************************************************************************/
/* Class used                                                           */
/************************************************************************/
class GameEngine;

/** 
* Resources used everywhere
*/
class CommonResources{

public:

	/** GameEngine instance	*/
	GameEngine* p_engine;

	/** Main font */
	CL_Font main_font;

	/** Graphic context */
	SDL_Renderer *p_gc;

	/** Human player */
	HumanPlayer player1;

	std::unique_ptr<Player> player2;

	/** Common player */
	Player* p_current_player;
	
	bool player2_support;

	/** Front layer */
	FrontLayer front_layer;

	/** Keys most used */
	Keys key;

	/** Current skin */
	std::string skin;
	zip_t *skin_zip;

	/** Pieces dimension */
	int pieces_width, pieces_height;
	int pieces_preview_width, pieces_preview_height;

	/** Time elapsed between current frame and the last one	*/
	float delta_time;

	/** Framerate (in frame / sec) */
	int fps;

	Statistics statistics;

	SDL_Color normal_color;
	SDL_Color selected_color;
	SDL_Color disabled_color;

	/** Constructor */
	CommonResources();

	/** Destructor */
	~CommonResources();
	
	/** Init resources */
	void init(GameEngine* p_engine);

	/** Load GFX */
	void load_gfx(SDL_Renderer *gc, std::string skin);

	SDL_Texture *load_texture(SDL_Renderer *gc, std::string filename, int &w, int &h);

	SDL_Texture *load_texture(zip_t *zip_file, SDL_Renderer *gc, std::string filename, int &w, int &h);

	void release_texture(SDL_Texture *p);

	/** Unload GFX */
	void unload_gfx();

	/** Read scores	*/
	void read_scores();

	/** Save scores	*/
	void save_scores();

};

/** 
* Init common resources
*/
void common_resources_init();

/** 
* Terminate common resources
*/
void common_resources_term();

/** 
* Get the common resources singleton instance
*/
CommonResources* common_resources_get_instance();

#endif
