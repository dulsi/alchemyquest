// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : Skin.h
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _ELEMENT_H_
#define _ELEMENT_H_

#include <SDL.h>
#include "SDL_CL_Sprite.h"
#include <string>

/** A skin class */
class Element{

public:

	/** Constructor	*/
	Element(std::string fullpath, std::string filename, SDL_Renderer *gc);

	/** Get filename */
	inline std::string get_filename() const { return _filename; }

	/** Get logo */
	inline std::string get_name() { return _name; }

private:

	/** Element filename */
	std::string _filename;

	/** Name */
	std::string _name; 

};

#endif
