// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : Preferences.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include <SDL.h>

#include "Preferences.h"
#include "IniFile.h"
#include "misc.h"
#include "memory.h"

/************************************************************************/
/* Singleton                                                            */
/************************************************************************/
Preferences* pref_get_instance()
{
	static Preferences* p_instance = NULL;
	if(p_instance == NULL)
		p_instance = my_new Preferences();
	return p_instance;
}

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
Preferences::Preferences()
{
	read();
}

/************************************************************************/
/* Read                                                                 */
/************************************************************************/
void Preferences::read()
{
	std::string options_path = get_save_path();
	std::string options_file = get_save_path() + get_path_separator() +
		"preferences.ini";
	_set_default();

	FILE *file = fopen(options_file.c_str(), "rb");
	if (file == NULL)
	{
		_set_default();
		write();
	}
	else
	{
		_read_options_file(file);
		fclose(file);
	}
}

/************************************************************************/
/* Write                                                                */
/************************************************************************/
void Preferences::write()
{

	std::string options_path = get_save_path();
	std::string options_file = options_path + get_path_separator() +
		"preferences.ini";

	FILE *file = fopen(options_file.c_str(), "wb");
	if (file == NULL)
	{
		std::cout << "Can't write file " << options_file <<".\n";
	}
	else
	{
		_write_options_file(file);
		fclose(file);
	}
}


/************************************************************************/
/* Read options                                                         */
/************************************************************************/
void Preferences::_read_options_file(FILE* p_file)
{
	IniFile ini;
	ini.read(p_file);

	render_target = HARDWARE;

	std::string default_target = "HARDWARE";
	std::string rt = ini.get("Render Target", default_target);
	if(rt == "HARDWARE")
	{
		render_target = Preferences::HARDWARE;
	}
	else if(rt == "SOFTWARE")
	{
		render_target = Preferences::SOFTWARE;
	}

	fullscreen = ini.get("Fullscreen", fullscreen);
	scale = ini.get("Scale", scale);
	sound_level = ini.get("Sound Level", sound_level);
	music_level = ini.get("Music Level", music_level);
	maxfps = ini.get("MaxFPS", maxfps);
	colorblind = ini.get("Colorblind", colorblind);

	std::string skin_file = ini.get("Skin", skin);
	FILE *test_file = fopen(skin_file.c_str(), "rb");
	if (test_file)
	{
		skin = skin_file;
		fclose(test_file);
	}
	else
	{
		std::cout << "Skin " << skin_file <<
			" was not found or is not a zip file, we use " <<
			skin << " instead."  << std::endl;
	}
}

/************************************************************************/
/* Write options                                                        */
/************************************************************************/
void Preferences::_write_options_file(FILE* p_file)
{

	IniFile ini;
	ini.clear();

	std::string rt = "SOFTWARE";
	switch(render_target)
	{
	case SOFTWARE:
		rt = "SOFTWARE";
		break;
	case HARDWARE:
		rt = "HARDWARE";
		break;
	}

	ini.add("Render Target", rt);
	ini.add("Fullscreen", fullscreen);
	ini.add("Scale", scale);
	ini.add("Sound Level", sound_level);
	ini.add("Music Level", music_level);
	ini.add("MaxFPS", maxfps);
	ini.add("Colorblind", colorblind);
	ini.add("Skin", skin);

	ini.write(p_file, "Preferences");
}

/************************************************************************/
/* Set Default                                                          */
/************************************************************************/
void Preferences::_set_default()
{
	render_target = HARDWARE;
	maxfps = 60;
	sound_level = 100;
	music_level = 100;
	fullscreen = false;
	scale = (alchemyQuest ? 2 : 1);
	colorblind = false;
	skin = get_skins_path() + get_path_separator() + "aqua.zip";
}
