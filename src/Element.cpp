// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : Element.cpp
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "Element.h"
#include "CommonResources.h"
#include "Preferences.h"
#include <stdexcept>

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
Element::Element(std::string fullpath, std::string filename, SDL_Renderer *gc)
{
	_filename = filename;
	zip_t *element_zip = zip_open(fullpath.c_str(), ZIP_RDONLY, NULL);
	CL_ResourceManager gfx(element_zip, "pieces.xml");
	_name = gfx.get_string_resource("name", "");
	zip_close(element_zip);
}
