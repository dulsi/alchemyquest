// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : Skin.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "Skin.h"
#include "CommonResources.h"
#include "Preferences.h"
#include <stdexcept>

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
Skin::Skin(std::string filename, SDL_Renderer *gc)
{
	_filename = filename;
	_element = 3;
	load_logo(gc);
}


void Skin::load_logo(SDL_Renderer *gc)
{
	zip_t *skin_zip = zip_open(_filename.c_str(), ZIP_RDONLY, NULL);
	if (skin_zip == NULL)
		throw std::runtime_error(_filename + " is not found");
	// We load the logo sprite in the gfx ressources file
	CL_ResourceManager gfx(skin_zip, "general.xml");
	_logo = CL_Image(gc, "logo", &gfx);
	zip_close(skin_zip);
}
