// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : PauseMenuState.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _PAUSEMENU_STATE_H_
#define _PAUSEMENU_STATE_H_

#include "MenuState.h"
#include "MenuState/BasicItem.h"
#include "MenuState/BasicTextItem.h"
#include "../KeyboardKey.h"
#include "GeneralMenuState.h"

/**
* PauseMenu State
*/
class PauseMenuState : public GeneralMenuState{

public:

	/** Constructor */
	PauseMenuState();

private:
};

#endif
