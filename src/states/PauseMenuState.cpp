// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : PauseMenuState.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "../memory.h"
#include <SDL.h>

#include "PauseMenuState.h"
#include "../CommonResources.h"
#include "../GameEngine.h"
#include "../misc.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
PauseMenuState::PauseMenuState ():
GeneralMenuState("menu_pause.xml")
{
}
