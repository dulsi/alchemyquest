// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : MenuState.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _MENU_STATE_H_
#define _MENU_STATE_H_

#include <SDL.h>
#include "../SDL_CL_Sprite.h"
#include "../SDL_CL_ResourceManager.h"
#include <vector>

#include "GameState.h"
#include "../KeyboardKey.h"
#include "../Player.h"

/** Action performed type */
enum ActionType{
	ACTION_TYPE_ENTER,
	ACTION_TYPE_LEFT,
	ACTION_TYPE_RIGHT,
	ACTION_TYPE_UP,
	ACTION_TYPE_DOWN,
	ACTION_TYPE_MOUSE
};

class MenuItem;
class ChoiceItem;

/**
* Menu State - a based class for all menus
*/
class MenuState : public GameState{

public:

	/** Constructor */
	MenuState();

	virtual void init() = 0;

	virtual void term() = 0;

	virtual void load_gfx(SDL_Renderer *gc) = 0;

	virtual void unload_gfx() = 0;

	virtual void draw(SDL_Renderer *gc);

	virtual void update(SDL_Renderer *gc);

	virtual void events(GameWindow& window);

	virtual bool front_layer_behind();
	
	/** Action performed - propaged to derivated class */
	virtual void action_performed(std::shared_ptr<MenuItem> item, ActionType action_type) = 0;

	/** Update derivated class */
	virtual void update_child() = 0;
	
	/** Start the menu */
	void start();

	/** Set if user can quit the menu or not */
	inline void set_quit_enable(bool enable){ _quit_enabled = enable; }
 
	MenuItem *load_basic_menu_item(SDL_Renderer *gc, CL_ResourceManager &gfx, const std::string &basename, const std::string &action, int x, int y);

	ChoiceItem *load_basic_multiple_choices_item(SDL_Renderer *gc, CL_ResourceManager &gfx, const std::string &basename, const std::string &action, const std::string &basechoiceposition, const std::string &basechoice, int x, int y);

	ChoiceItem *load_dual_choice_item(SDL_Renderer *gc, CL_ResourceManager &gfx, const std::string &yesname, const std::string &noname);

protected:

	/** Menu state (is it appearing, active, disappearing...) */
	int _state;

	/** Alpha value of the menu */
	float _alpha;

	/** Menu Items (widgets) */
	std::vector<std::shared_ptr<MenuItem> > _items[2];

	/** Full background sprite */
	CL_Sprite *_full_background;

	/** Background sprite */
	CL_Sprite _background;

	/** Background label */
	std::string _bgLabel;

	/** Current selection inside the menu */
	int _selection[2];

	/** X mouse coord */
	int _mouse_x;

	/** Y mouse coord */
	int _mouse_y;

	/** Is mouse button clicked */
	bool _mouse_is_clicked;

	/** If user can quit the menu */
	bool _quit_enabled;

	/** Set background sprite */
	void _set_background_sprite(CL_Sprite& background);

	/** Make menu appear */
	void _appear();

	/** Make menu disappear */
	void _disappear();

	void _events(GameWindow& window, std::vector<std::shared_ptr<MenuItem> > &items, int &selection, Player &player);

	/** Start menu apparition */
	void _start_disappear();

	/** Mouse events */
	void _mouse_events(GameWindow& window);

};

#endif
