// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : GameOverState.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "GameOverState.h"
#include "../CommonResources.h"
#include "../GameEngine.h"
#include "../misc.h"
#include "../Window.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
GameOverState::GameOverState()
{
	_is_highscore = false;
	_quit_enabled = false;
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
GameOverState::~GameOverState()
{
	unload_gfx();
}

/************************************************************************/
/* Init                                                                 */
/************************************************************************/
void GameOverState::init()
{
}

/************************************************************************/
/* Term                                                                 */
/************************************************************************/
void GameOverState::term()
{

}

/************************************************************************/
/* Load GFX                                                             */
/************************************************************************/
void GameOverState::load_gfx(SDL_Renderer *gc)
{
	unload_gfx();

	// Getting skins resources
	CL_ResourceManager gfx("menu_gameover.xml");

	_dialog_gameover = CL_Sprite(gc, "menu_gameover/dialog_gameover", &gfx);
	_dialog_highscore = CL_Sprite(gc, "menu_gameover/dialog_highscore", &gfx);
	_bgLabel = gfx.get_string_resource("menu_gameover/label", "");
	_your_score = gfx.get_string_resource("menu_gameover/label_yourscore", "");
	_high_score = gfx.get_string_resource("menu_gameover/label_highscore", "");
	_new_high_score = gfx.get_string_resource("menu_gameover/label_new_highscore", "");
	_older = gfx.get_string_resource("menu_gameover/label_older", "");
	_play_again = gfx.get_string_resource("menu_gameover/label_playagain", "");
	_scoretext_y = CL_Integer_to_int("menu_gameover/score_text_top", &gfx);
	_highscoretext_y = CL_Integer_to_int("menu_gameover/highscore_text_top", &gfx);
	_playagaintext_y = CL_Integer_to_int("menu_gameover/playagain_text_top", &gfx);

	_score1_x = CL_Integer_to_int("menu_gameover/score1_left", &gfx);
	_score1_y = CL_Integer_to_int("menu_gameover/score1_top", &gfx);
	_score2_x = CL_Integer_to_int("menu_gameover/score2_left", &gfx);
	_score2_y = CL_Integer_to_int("menu_gameover/score2_top", &gfx);

	_quit_choice_item = load_dual_choice_item(gc, gfx, "menu_gameover/new_game_question/yes/", "menu_gameover/new_game_question/no/");
}

/************************************************************************/
/* Unload GFX                                                           */
/************************************************************************/
void GameOverState::unload_gfx()
{
	_items[0].clear();
}

/************************************************************************/
/* Draw                                                                 */
/************************************************************************/
void GameOverState::draw(SDL_Renderer *gc)
{
	MenuState::draw(gc);

	int x = ((alchemyQuest ? ALCHEMYQUEST_WIDTH : GAME_WIDTH) / 2) - _background.get_width () / 2;
	int y = ((alchemyQuest ? ALCHEMYQUEST_HEIGHT : GAME_HEIGHT) / 2) - _background.get_height () / 2;
	if (x < 0)
		x = 0;
	if (y < 0)
		y = 0;
	if(_is_highscore)
	{
		if (!_new_high_score.empty())
		{
			int textSize_width = _p_common_resources -> main_font.get_text_width(gc, _new_high_score);
			_p_common_resources -> main_font.draw_text(gc, x + _background.get_width () / 2 - (textSize_width / 2), _scoretext_y, _new_high_score);
		}
		if (!_older.empty())
		{
			int textSize_width = _p_common_resources -> main_font.get_text_width(gc, _older);
			_p_common_resources -> main_font.draw_text(gc, x + _background.get_width () / 2 - (textSize_width / 2), _highscoretext_y, _older);
		}
		if (!_play_again.empty())
		{
			int textSize_width = _p_common_resources -> main_font.get_text_width(gc, _play_again);
			_p_common_resources -> main_font.draw_text(gc, x + _background.get_width () / 2 - (textSize_width / 2), _playagaintext_y, _play_again);
		}
		std::string new_score = format_number(to_string(_p_common_resources -> statistics.highscore));
		std::string old_score = format_number(to_string(_p_common_resources -> statistics.old_highscore));

		int new_score_real_x = _score1_x -
			_p_common_resources -> main_font.get_text_width(gc, new_score) / 2;

		int old_score_real_x = _score2_x - 
			_p_common_resources -> main_font.get_text_width(gc, old_score) / 2;

		_p_common_resources -> main_font.draw_text(gc, new_score_real_x, _score1_y, new_score);
		_p_common_resources -> main_font.draw_text(gc, old_score_real_x, _score2_y, old_score);
	}
	else
	{
		if (!_your_score.empty())
		{
			int textSize_width = _p_common_resources -> main_font.get_text_width(gc, _your_score);
			_p_common_resources -> main_font.draw_text(gc, x + _background.get_width () / 2 - (textSize_width / 2), _scoretext_y, _your_score);
		}
		if (!_high_score.empty())
		{
			int textSize_width = _p_common_resources -> main_font.get_text_width(gc, _high_score);
			_p_common_resources -> main_font.draw_text(gc, x + _background.get_width () / 2 - (textSize_width / 2), _highscoretext_y, _high_score);
		}
		if (!_play_again.empty())
		{
			int textSize_width = _p_common_resources -> main_font.get_text_width(gc, _play_again);
			_p_common_resources -> main_font.draw_text(gc, x + _background.get_width () / 2 - (textSize_width / 2), _playagaintext_y, _play_again);
		}
		std::string current_score = format_number(to_string(_p_common_resources -> player1.get_score()));
		std::string highscore = format_number(to_string(_p_common_resources -> statistics.highscore));

		int score1_real_x = _score1_x -
			_p_common_resources -> main_font.get_text_width(gc, current_score) / 2;

		int score2_real_x = _score2_x - 
			_p_common_resources -> main_font.get_text_width(gc, highscore) / 2;

		_p_common_resources -> main_font.draw_text(gc, score1_real_x, _score1_y, current_score);
		_p_common_resources -> main_font.draw_text(gc, score2_real_x, _score2_y, highscore);
	}
}

/************************************************************************/
/* Events                                                               */
/************************************************************************/
void GameOverState::events(GameWindow& window)
{
	MenuState::events(window);

	if(_p_common_resources -> key.retry.get())
	{
		_p_common_resources -> p_engine -> stop_current_state();
		_p_common_resources -> p_engine -> set_state_ingame();
		_p_common_resources -> player1.new_game();
	}

	if(_p_common_resources -> key.undo.get())
	{
		_p_common_resources -> p_engine -> set_state_ingame();
		_p_common_resources -> player1.undo();
	}

	if(_p_common_resources->key.skins.get())
	{
		_p_common_resources -> p_engine -> set_state_skin_menu();
	}
}

/************************************************************************/
/* Set mode                                                             */
/************************************************************************/
void GameOverState::set_highscore(bool highscore)
{
	_is_highscore = highscore;
	if(_is_highscore)
	{
		_background = _dialog_highscore;
	}
	else
	{
		_background = _dialog_gameover;
	}
}

/************************************************************************/
/* Action performed                                                     */
/************************************************************************/
void GameOverState::action_performed(std::shared_ptr<MenuItem> item, ActionType action_type)
{
	switch(action_type)
	{
	case ACTION_TYPE_ENTER:
	case ACTION_TYPE_MOUSE:
		if(_quit_choice_item->get_current_choice() == CHOICE_LEFT)
		{
			_p_common_resources -> p_engine -> stop_current_state();
			_p_common_resources -> p_engine -> set_state_ingame();
			_p_common_resources -> player1.new_game();
			if (_p_common_resources->player2.get())
				_p_common_resources -> player2->new_game();
		}
		else
		{
			_p_common_resources -> p_engine -> stop_current_state();
			_p_common_resources -> p_engine -> set_state_title();
			_p_common_resources->player1.load_gfx(_p_common_resources->p_gc, "player1/");
			_p_common_resources -> player2 = NULL;
		}
		break;
	default:
		break;
	}
}

/************************************************************************/
/* Update child                                                         */
/************************************************************************/
void GameOverState::update_child()
{
	
}
