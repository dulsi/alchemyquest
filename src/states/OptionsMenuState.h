// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : OptionsMenuState.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _OPTIONSMENU_STATE_H_
#define _OPTIONSMENU_STATE_H_

#include <SDL.h>

#include "MenuState.h"
#include "MenuState/BasicItem.h"
#include "MenuState/MultipleChoicesItem.h"
#include "MenuState/MultipleTextChoicesItem.h"
#include "GeneralMenuState.h"

class GameEngine;

/**
* OptionsMenu State
*/
class OptionsMenuState : public GeneralMenuState
{

public:

	/** Constructor */
	OptionsMenuState();

private:
};

#endif
