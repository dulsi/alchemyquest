// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : CommonState.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _COMMON_STATE_H_
#define _COMMON_STATE_H_

#include <SDL.h>

#include "GameState.h"
#include "../KeyboardKey.h"

class GameEngine;

/**
* Common State - the state which is always active.
*/
class CommonState : public GameState{

public:

	/** Constructor */
	CommonState();

	/** Destructor */
	~CommonState();

	virtual void init();

	virtual void term();

	virtual void load_gfx(SDL_Renderer *gc);

	virtual void unload_gfx();

	virtual void draw(SDL_Renderer *gc);
	
	virtual void update(SDL_Renderer *gc);
	
	virtual void events(GameWindow& window);

	virtual bool front_layer_behind();

private:

	/** Background image */
	CL_Image _background;

	/** Background image for 2 players */
	CL_Image _background_2p;

	/** Enable Framerate drawing */
	bool _enable_framerate_drawing;

};

#endif
