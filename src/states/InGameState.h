// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : InGameState.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _INGAME_STATE_H_
#define _INGAME_STATE_H_

#include <SDL.h>

#include "GameState.h"
#include "../KeyboardKey.h"

class GameEngine;

/**
* InGame State - main state
*/
class InGameState : public GameState{

public:

	/** Constructor */
	InGameState();

	/** Destructor */
	~InGameState();
	
	virtual void init();

	virtual void term();
	
	virtual void load_gfx(SDL_Renderer *gc);
	
	virtual void unload_gfx();

	virtual void draw(SDL_Renderer *gc);
	
	virtual void update(SDL_Renderer *gc);
	
	virtual void events(GameWindow& window);

	virtual bool front_layer_behind();

};

#endif
