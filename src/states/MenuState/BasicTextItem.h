// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : BasicTextItem.h
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _BASIC_TEXT_ITEM_H_
#define _BASIC_TEXT_ITEM_H_

#include <SDL.h>

#include "MenuItem.h"

/**
* Basic Text Item class
*/
class BasicTextItem : public MenuItem{

public:

	/** Constructor */
	BasicTextItem();	

	/** Destructor */
	~BasicTextItem();
	
	/** Unload GFX */
	void unload_gfx();
	
	virtual void draw(SDL_Renderer *gc);
	
	virtual void action_performed(ActionType action_type, Player &player);
	
	virtual void mouse_moved(int mouse_x, int mouse_y);
	
	virtual bool quit_menu_on_action(){ return (_action == ACTION_READY ? false : true); }
	
	virtual bool is_inside(int x, int y);

	/** Set width */
	void set_w(int w);

	/** Set height */
	void set_h(int h);
	
	/** Set text */
	void set_text(std::string text);

protected:

	/** Default sprite */
	std::string _text;

	/** Item size */
	int _w, _h;

};

#endif
