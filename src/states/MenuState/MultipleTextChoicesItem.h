// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : MultipleTextChoiceItem.h
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _MULTIPLE_TEXT_CHOICES_ITEM_H_
#define _MULTIPLE_TEXT_CHOICES_ITEM_H_

#include <vector>
#include <SDL.h>

#include "ChoiceItem.h"

/**
* Multiples choices item
*/
class MultipleTextChoicesItem : public ChoiceItem {

public:

	/** Constructor */
	MultipleTextChoicesItem();

	/** Destructor */
	~MultipleTextChoicesItem();	

	virtual void draw(SDL_Renderer *gc);

	virtual void action_performed(ActionType action_type, Player &player);

	virtual bool quit_menu_on_action(){return false;}

	virtual bool is_inside(int x, int y);

	virtual void mouse_moved(int mouse_x, int mouse_y);

	/** Set choices X coord */
	void set_choice_x(int x){_choice_x = x;}

	/** Set choices Y coord */
	void set_choice_y(int y){_choice_y = y;}

	/** Add a choice */
	void add_choice(std::string name);

	/** Return current choice */
	unsigned int get_number_choices();

	/** Clear choices */
	void clear_choices();

	/** Unload GFX */
	void unload_gfx();

	/** Set width */
	void set_w(int w);

	/** Set height */
	void set_h(int h);
	
	/** Set text */
	void set_text(std::string text);

protected:

	/** Default sprite */
	std::string _text;

	/** Item size */
	int _w, _h;

	/** Choices list */
	std::vector<std::string> _choices_list;

	/** Choices X coord */
	int _choice_x;

	/** Choices Y coord */
	int _choice_y;

};



#endif
