// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : MultipleTextChoicesItem.cpp
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include <iostream>
#include "MultipleTextChoicesItem.h"
#include "../../memory.h"
#include "../../CommonResources.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
MultipleTextChoicesItem::MultipleTextChoicesItem()
{
	_choices_list.clear();
	_selection = 0;
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
MultipleTextChoicesItem::~MultipleTextChoicesItem()
{
	unload_gfx();
}

/************************************************************************/
/* Unload GFX                                                           */
/************************************************************************/
void MultipleTextChoicesItem::unload_gfx()
{
	_choices_list.clear();
}

/************************************************************************/
/* Is inside                                                            */
/************************************************************************/
bool MultipleTextChoicesItem::is_inside(int x, int y)
{
	return x >= _choice_x && x <= _choice_x + _w &&
		y >= _y && y <= _y + _h;
}

/************************************************************************/
/* Add choice                                                           */
/************************************************************************/
void MultipleTextChoicesItem::add_choice(std::string name)
{
	_choices_list.push_back(name);
}

/************************************************************************/
/* Get number of choices                                                */
/************************************************************************/
unsigned int MultipleTextChoicesItem::get_number_choices()
{
	return _choices_list.size();
}

/************************************************************************/
/* Clear choices                                                        */
/************************************************************************/
void MultipleTextChoicesItem::clear_choices()
{    
	_choices_list.clear();
}

/************************************************************************/
/* Draw                                                                 */
/************************************************************************/
void MultipleTextChoicesItem::draw(SDL_Renderer *gc)
{
	// Getting resources
	static CommonResources* p_resources = common_resources_get_instance();
	if (_is_selected)
	{
		p_resources -> main_font.draw_text(gc, _x, _y + _h, _text, p_resources->selected_color);
		if(_selection < _choices_list.size())
		{
			p_resources -> main_font.draw_text(gc, _choice_x, _choice_y + _h, _choices_list[_selection], p_resources->selected_color);
		}
	}
	else if (is_locked())
	{
		p_resources -> main_font.draw_text(gc, _x, _y + _h, _text, p_resources->disabled_color);
		if(_selection < _choices_list.size())
		{
			p_resources -> main_font.draw_text(gc, _choice_x, _choice_y + _h, _choices_list[_selection], p_resources->disabled_color);
		}
	}
	else
	{
		p_resources -> main_font.draw_text(gc, _x, _y + _h, _text, p_resources->normal_color);
		if(_selection < _choices_list.size())
		{
			p_resources -> main_font.draw_text(gc, _choice_x, _choice_y + _h, _choices_list[_selection], p_resources->normal_color);
		}
	}
}

/************************************************************************/
/* Action performed                                                     */
/************************************************************************/
void MultipleTextChoicesItem::action_performed(ActionType action_type, Player &player)
{
	if(ACTION_TYPE_LEFT == action_type && _selection > 0)
	{
		_selection --;
	}
	else if(ACTION_TYPE_RIGHT == action_type &&
		_selection < _choices_list.size()-1)
	{
		_selection ++;
	}
	else if(ACTION_TYPE_MOUSE == action_type)
	{
		_selection++;
		_selection = _selection % _choices_list.size();
	}

}

/************************************************************************/
/* Mouse moved                                                          */
/************************************************************************/
void MultipleTextChoicesItem::mouse_moved(int mouse_x, int mouse_y)
{

}

void MultipleTextChoicesItem::set_w(int w)
{
	_w = w;
}

void MultipleTextChoicesItem::set_h(int h)
{
	_h = h;
}

void MultipleTextChoicesItem::set_text(std::string text)
{
	_text = text;
}
