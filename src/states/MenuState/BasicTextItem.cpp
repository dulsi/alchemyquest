// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : BasicTextItem.cpp
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "../../memory.h"
#include "../../CommonResources.h"
#include <iostream>
#include "BasicTextItem.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
BasicTextItem::BasicTextItem()
{
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
BasicTextItem::~BasicTextItem()
{
	unload_gfx();
}

/************************************************************************/
/* Unload GFX                                                           */
/************************************************************************/
void BasicTextItem::unload_gfx()
{

}

/************************************************************************/
/* Draw                                                                 */
/************************************************************************/
void BasicTextItem::draw(SDL_Renderer *gc)
{
	// Getting resources
	static CommonResources* p_resources = common_resources_get_instance();
	if (_is_selected)
	{
		p_resources -> main_font.draw_text(gc, _x, _y + _h, _text, p_resources->selected_color);
	}
	else if (is_locked())
	{
		p_resources -> main_font.draw_text(gc, _x, _y + _h, _text, p_resources->disabled_color);
	}
	else
	{
		p_resources -> main_font.draw_text(gc, _x, _y + _h, _text, p_resources->normal_color);
	}
}

/************************************************************************/
/* Action performed                                                     */
/************************************************************************/
void BasicTextItem::action_performed(ActionType action_type, Player &player)
{
	if ((action_type == ACTION_TYPE_ENTER) && (get_action() == ACTION_READY))
		player.set_ready(true);
	if ((action_type == ACTION_TYPE_MOUSE) && (get_action() == ACTION_READY))
		player.set_ready(true);
}

/************************************************************************/
/* Is inside                                                            */
/************************************************************************/
bool BasicTextItem::is_inside(int x, int y)
{
	return x >= _x && x <= _x + _w &&
		y >= _y && y <= _y + _h;
}

/************************************************************************/
/* Mouse moved                                                          */
/************************************************************************/
void BasicTextItem::mouse_moved(int mouse_x, int mouse_y)
{

}

void BasicTextItem::set_w(int w)
{
	_w = w;
}

void BasicTextItem::set_h(int h)
{
	_h = h;
}

void BasicTextItem::set_text(std::string text)
{
	_text = text;
}
