// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : DualChoiceTextItem.cpp
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include <iostream>
#include "../../CommonResources.h"
#include "DualChoiceTextItem.h"
#include "../../memory.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
DualChoiceTextItem::DualChoiceTextItem()
{
	_selection = CHOICE_LEFT;
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
DualChoiceTextItem::~DualChoiceTextItem()
{
}

/************************************************************************/
/* Set X2                                                               */
/************************************************************************/
void DualChoiceTextItem::set_x2(int x)
{
	_x2 = x;
}

/************************************************************************/
/* Set Y2                                                               */
/************************************************************************/
void DualChoiceTextItem::set_y2(int y)
{
	_y2 = y;
}

/************************************************************************/
/* Set Text                                                             */
/************************************************************************/
void DualChoiceTextItem::set_text(std::string left_text, std::string right_text)
{
	_left_text = left_text;
	_right_text = right_text;
}

/************************************************************************/
/* Draw                                                                 */
/************************************************************************/
void DualChoiceTextItem::draw(SDL_Renderer *gc)
{
	static CommonResources* p_resources = common_resources_get_instance();
	_left_size_height = p_resources -> main_font.get_text_height(gc, _left_text);
	_right_size_height = p_resources -> main_font.get_text_height(gc, _right_text);
	_left_size_width = p_resources -> main_font.get_text_width(gc, _left_text);
	_right_size_width = p_resources -> main_font.get_text_width(gc, _right_text);
	if(_selection == CHOICE_LEFT)
	{
		p_resources -> main_font.draw_text(gc, _x, _y + _left_size_height, _left_text, p_resources->selected_color);
		p_resources -> main_font.draw_text(gc, _x2, _y2 + _right_size_height, _right_text, p_resources->normal_color);
	}
	else
	{
		p_resources -> main_font.draw_text(gc, _x, _y + _left_size_height, _left_text, p_resources->normal_color);
		p_resources -> main_font.draw_text(gc, _x2, _y2 + _right_size_height, _right_text, p_resources->selected_color);
	}
}

/************************************************************************/
/* Action performed                                                     */
/************************************************************************/
void DualChoiceTextItem::action_performed(ActionType action_type, Player &player)
{
	if(ACTION_TYPE_LEFT == action_type)
	{
		_selection = CHOICE_LEFT;
	}

	if(ACTION_TYPE_RIGHT == action_type)
	{
		_selection = CHOICE_RIGHT;
	}
}

/************************************************************************/
/* Is inside                                                            */
/************************************************************************/
bool DualChoiceTextItem::is_inside(int x, int y)
{
	return x >= _x && x <= _x2 + _right_size_width &&
		y >= _y && y <= _y2 + _right_size_height;
}

/************************************************************************/
/* Mouse moved                                                          */
/************************************************************************/
void DualChoiceTextItem::mouse_moved(int mouse_x, int mouse_y)
{
	if(mouse_x <= _x + _left_size_width)
	{
		_selection = CHOICE_LEFT;
	}
	else if(mouse_x >= _x2)
	{
		_selection = CHOICE_RIGHT;
	}
}

/************************************************************************/
/* Get the number of choices                                            */
/************************************************************************/
unsigned int DualChoiceTextItem::get_number_choices()
{
	return 2;
}
