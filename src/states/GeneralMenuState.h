// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : GeneralMenuState.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _GENERALMENU_STATE_H_
#define _GENERALMENU_STATE_H_

#include "MenuState.h"
#include "MenuState/BasicItem.h"
#include "MenuState/BasicTextItem.h"
#include "../KeyboardKey.h"

/**
* PauseMenu State
*/
class GeneralMenuState : public MenuState{

public:

	/** Constructor */
	GeneralMenuState(const std::string &xmlfile);

	/** Destructor */
	~GeneralMenuState();

	virtual void init();

	virtual void term();

	virtual void load_gfx(SDL_Renderer *gc);

	virtual void unload_gfx();

	virtual void action_performed(std::shared_ptr<MenuItem> item, ActionType action_type);

	virtual void update_child();	

	/** Toggle screen */
	void toggle_screen();

protected:
	void load_general_menu(SDL_Renderer *gc, CL_ResourceManager &gfx, int x, int y, const std::vector<std::string> &menu, std::vector<std::shared_ptr<MenuItem> > &items);

	void populate_values(std::vector<std::shared_ptr<MenuItem> > &items, const Player &player);

	void load_gfx_compatibility(SDL_Renderer *gc, CL_ResourceManager &gfx);

private:
	std::string _xmlfile;
};

#endif
