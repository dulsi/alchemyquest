// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : CommonState.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include <SDL.h>
#include <SDL_mixer.h>

#include "../memory.h"
#include "CommonState.h"
#include "../GameEngine.h"
#include "../CommonResources.h"
#include "../KeyboardKey.h"
#include "../misc.h"
#include "../Preferences.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
CommonState::CommonState()
{
	_enable_framerate_drawing = false;
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
CommonState::~CommonState()
{
	unload_gfx();
	term();
}

/************************************************************************/
/* Init                                                                 */
/************************************************************************/
void CommonState::init()
{
}

/************************************************************************/
/* Terminate                                                            */
/************************************************************************/
void CommonState::term()
{
}

/************************************************************************/
/* Load GFX                                                             */
/************************************************************************/
void CommonState::load_gfx(SDL_Renderer *gc)
{
	unload_gfx();

	CL_ResourceManager gfx("general.xml");

	_background = CL_Image(gc, "background", &gfx);
	if (gfx.resource_exists("background_2p"))
		_background_2p = CL_Image(gc, "background_2p", &gfx);
}

/************************************************************************/
/* Unload GFX                                                           */
/************************************************************************/
void CommonState::unload_gfx()
{

}

/************************************************************************/
/* Draw                                                                 */
/************************************************************************/
void CommonState::draw(SDL_Renderer *gc)
{
	SDL_SetRenderDrawColor(gc, 0, 0, 0, 255);
	SDL_RenderClear(gc);
	if ((_p_common_resources->player2.get()) && (!_background_2p.is_null()))
		_background_2p.draw(gc, 0, 0);
	else
		_background.draw(gc, 0, 0);
	if(_enable_framerate_drawing)
	{
		_p_common_resources -> main_font.draw_text(gc, 580.0f,580.0f,
			to_string(_p_common_resources -> p_engine -> get_fps()));
	}

	if(_p_common_resources -> p_current_player)
		_p_common_resources -> p_current_player -> draw(gc);
	if(_p_common_resources->player2.get())
		_p_common_resources->player2->draw(gc);
}

/************************************************************************/
/* Update                                                               */
/************************************************************************/
void CommonState::update(SDL_Renderer *gc)
{

}

/************************************************************************/
/* Events                                                               */
/************************************************************************/
void CommonState::events(GameWindow& window)
{
	SDL_Event sdlevent;
	int avail = 0;
	for (avail = SDL_PollEvent(&sdlevent); avail; avail = SDL_PollEvent(&sdlevent))
	{
		if ((sdlevent.type == SDL_QUIT) || (_p_common_resources->key.num1.get() && _p_common_resources->key.num2.get()))
		{
			Mix_Quit();
			SDL_Quit();
			exit(0);
		}
	}
	if(_p_common_resources->key.framerate.get())
	{
		_enable_framerate_drawing = !_enable_framerate_drawing;
	}
	if(_p_common_resources->key.fullscreen.get())
	{
		_p_common_resources -> p_engine -> toggle_screen();
	}
}

/************************************************************************/
/* Front layer behind                                                   */
/************************************************************************/
bool CommonState::front_layer_behind()
{
	return false;
}
