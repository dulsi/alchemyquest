// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : AudioManager.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "AudioManager.h"
#include "Preferences.h"
#include "memory.h"
#include "misc.h"

/************************************************************************/
/* Global                                                               */
/************************************************************************/
AudioManager g_audio_manager;

/************************************************************************/
/* Constants                                                            */
/************************************************************************/
static const std::string SOUND_CREATION_FILE = "creation.wav";
static const std::string SOUND_DESTROY_FILE = "destroy.wav";
static const std::string SOUND_MOVE_FILE = "move.wav";
static const std::string SOUND_FALL_FILE = "fall.wav";
static const std::string MUSIC_FILE = "Cavern_Of_Time.ogg";

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
AudioManager::AudioManager()
{
	for(int i=0; i<NB_SOUNDS; ++i)
	{
		_sounds_p[i] = NULL;
	}
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
AudioManager::~AudioManager()
{
}

/************************************************************************/
/* Init                                                                 */
/************************************************************************/
void AudioManager::init()
{
	// Getting resources
	Preferences* p_pref = pref_get_instance();

	Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024);
	std::string dir_music = get_music_path();
	std::string dir_sound = get_sounds_path();
	_music_obj = Mix_LoadMUS((dir_music + get_path_separator()+ MUSIC_FILE).c_str());
	if (_music_obj)
		Mix_FadeInMusic(_music_obj, -1, 1000);
	Mix_VolumeMusic(MIX_MAX_VOLUME * p_pref -> music_level / 100);
	Mix_AllocateChannels(NB_SOUNDS);
	Mix_Volume(-1, MIX_MAX_VOLUME * p_pref -> sound_level / 100);
	_sounds_p[SOUND_MOVE] = Mix_LoadWAV((dir_sound+get_path_separator()+SOUND_MOVE_FILE).c_str());
	_sounds_p[SOUND_FALL] = Mix_LoadWAV((dir_sound+get_path_separator()+SOUND_FALL_FILE).c_str());
	_sounds_p[SOUND_CREATION] = Mix_LoadWAV((dir_sound+get_path_separator()+SOUND_CREATION_FILE).c_str());
	_sounds_p[SOUND_DESTROY] = Mix_LoadWAV((dir_sound+get_path_separator()+SOUND_DESTROY_FILE).c_str());
}

/************************************************************************/
/* Term                                                                 */
/************************************************************************/
void AudioManager::term()
{
	if (_music_obj)
	{
		Mix_HaltMusic();
		Mix_FreeMusic(_music_obj);
	}
	for(int i=0; i<NB_SOUNDS; ++i)
	{
		if(_sounds_p[i])
		{
			Mix_FreeChunk(_sounds_p[i]);
			_sounds_p[i] = NULL;
		}   
	}
	Mix_Quit();
}

/************************************************************************/
/* Set music volume                                                     */
/************************************************************************/
void AudioManager::set_music_volume(int volume)
{
	Mix_VolumeMusic(MIX_MAX_VOLUME * volume / 100);
}

/************************************************************************/
/* Set sounds volume                                                    */
/************************************************************************/
void AudioManager::set_sounds_volume(int volume)
{
	Mix_Volume(-1, MIX_MAX_VOLUME * volume / 100);
}

/************************************************************************/
/* Play sound                                                           */
/************************************************************************/
void AudioManager::play_sound(int sound_index)
{
	if(_sounds_p[sound_index])
	{
		Mix_PlayChannel(sound_index, _sounds_p[sound_index], 0);
	}
}

/************************************************************************/
/* Pause sounds                                                         */
/************************************************************************/
void AudioManager::pause_sounds()
{
	Mix_Pause(-1);
}

/************************************************************************/
/* Resume sounds                                                        */
/************************************************************************/
void AudioManager::resume_sounds()
{
	Mix_Resume(-1);
}
