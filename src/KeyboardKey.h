// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : KeyboardKey.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _KEYBOARDKEY_H_
#define _KEYBOARDKEY_H_

#include <SDL.h>

/**
* Class for keyboard key
*/
class KeyboardKey
{

public:

	/**
	* Constructor
	*/
	KeyboardKey(int key, bool repeat = false)
	{
		this->_key = key;
		this->_should_repeat = repeat;
		_is_key_active = true;
		_next_time = 0;
		_active = true;
	}

	/**
	* Return if the key is activated 
	*/
	bool get()
	{
		if (!_active)
			return false;
		const Uint8 *state = SDL_GetKeyboardState(NULL);
		if (state[_key])
		{
			if(!_is_key_active || (_should_repeat && SDL_GetTicks()>_next_time))
			{
				_is_key_active = true;
				_next_time = SDL_GetTicks()+380;
				return true;
			}
		}
		else
		{
			_is_key_active = false;
		}
		return false;
	}
	
	void set_active(bool val)
	{
		_active = val;
	}

private:
	
	/** Key code */
	int _key;

	/** Is the key pushed */
	bool _is_key_active;

	/** Is repeat enable */
	bool _should_repeat;

	/** Next repeat moment */
	unsigned long int _next_time;

	bool _active;
};

#endif
