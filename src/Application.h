// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : Application.h
//  Description : Application main class
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _APPLICATION_H_
#define _APPLICATION_H_

#include <string>
#include <vector>

/** Main application */
class Application{

public:

	/**
	* Application main function
	*/
	int app_main(const std::vector<std::string>& args);

private:

	/** 
	* Initialize application
	*/
	void _init();

	/** 
	* Terminate application
	*/
	void _term();

	/**
	* Check command lines arguments
	*/
	bool _check_args(const std::vector<std::string>& args);

	/**
	* Display the help informations
	*/
	void _print_help();

	/**
	* Display the license
	*/
	void _print_license();

};

#endif
