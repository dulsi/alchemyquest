// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : main.cpp
//  Description : Programme entry point
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "Application.h"

extern "C" 
{

int main(int argc, char *argv[])
{
	std::vector<std::string> args;
	for (int i = 0; i < argc; i++)
		args.push_back(argv[i]);
	Application application;
	return application.app_main(args);	
}

}
